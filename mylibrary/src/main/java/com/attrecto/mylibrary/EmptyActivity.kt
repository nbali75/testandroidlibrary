package com.attrecto.mylibrary

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

open class EmptyActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_empty)
    }
}
