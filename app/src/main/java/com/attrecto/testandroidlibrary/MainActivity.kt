package com.attrecto.testandroidlibrary

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.attrecto.mylibrary.AndroidAlma
import com.attrecto.mylibrary.EmptyActivity

class MainActivity : EmptyActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val androidAlma = AndroidAlma(applicationContext)
    }
}
